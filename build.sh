#!/usr/bin/env bash

# Start by formatting the code to standard
if [[ "$1" != "-noformat" ]]; then
    ./format.sh
fi

# Generate build environment
source configure.sh

if [[ -z $PLATFORM ]]; then
    "CANNOT CONTINUE"
    exit
fi

echo "++ Building"

OBJCOPY="avr-objcopy"
if [[ $PLATFORM == $WINDOWS ]]; then
	../3rdParty/tools/arduino-1.0.6-windows/hardware/tools/avr/utils/bin/make.exe
    OBJCOPY=$OBJCOPY".exe"
else
	make   
fi

RETURN=$?

if [ $RETURN = 0 ]; then
    $OBJCOPY -R .eeprom -R .fuse -R .lock -R .signature -O ihex SEG4545_Project SEG4545_Project.hex
    avr-size --format=avr --mcu=atmega2560 SEG4545_Project
else
    exit $RETURN
fi
