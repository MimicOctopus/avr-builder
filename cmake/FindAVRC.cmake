#
# Sets the following variables
#
# AVRC_INCLUDES
#
# AVRC_FOUND
#

# Make assumptions about paths
FIND_PATH(AVRC_INCLUDES stdlib.h PATHS "${CMAKE_SOURCE_DIR}/3rdParty/tools/arduino-1.0.6-windows/hardware/tools/avr/lib/gcc/avr/4.3.2/include/" "${CMAKE_SOURCE_DIR}/3rdParty/tools/arduino-1.0.6-linux/hardware/tools/avr/lib/avr/include/" NO_DEFAULT_PATH NO_CMAKE_SYSTEM_PATH)

IF(AVRC_INCLUDES)
	MESSAGE(STATUS "Found AVR CLib")

	MESSAGE(STATUS "AVRC_INCLUDES: ${AVRC_INCLUDES}")
	SET(AVR_FOUND TRUE)
ELSE()
	SET(AVR_FOUND FALSE)
	MESSAGE(FATAL "COULDN'T FIND AVRC")
ENDIF()

