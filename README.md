# avr-builder
A small build utilties suite for AVR based project, which uses cmake and bash

# Usage

Edit the files to represent your project structure.
When I built this, I unified the archive formats to only have to deal with one tool.
I hosted the files on github to control the downloading, but feel free to host it wherever or modify the scripts to use the official Arduino downloads

The scripts expect the libraries and other utilities go in the `3rdParty` folder.
The provided bash scripts will download the AVR utilities to the proper folders. You'll need to change some of them if you want to change the destination directories
Sources go in `src`

The build system (look at the code, it's relatively simple) can differentiate between Windows and Linux, and other systems could be added pretty easily if they support the Arduino Development kit.

# Why?

I was working on an AVR+FreeRTOS for school, and the Eclipse-based build system was being a pain in the ass.
Eclipse CDT also seems to be terrible at caching and has also become pretty heavy over the years, and that lead to many problems for the team. Add to that the fact that you *cannot* automate a Eclipse build through CI and it became relatively clear that we needed something better.

Updated in January 2017 to help some friends
