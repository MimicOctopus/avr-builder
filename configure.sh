#!/usr/bin/env bash

source env_setup.sh

if [[ -z $PLATFORM ]]; then
    "CANNOT CONTINUE"
    exit
fi

echo "-- Finding Sources"
find src -name '*.[ch]' > sources.txt

echo "-- Generating build config"
mkdir -p build
cd build
cmake -G "Unix Makefiles" ..
