#!/usr/bin/env bash

source env_setup.sh

cp res/clang-format .clang-format
if [[ $PLATFORM == $WINDOWS ]]; then
    echo "Patching clang-format on windows"
    cp res/clang-format-win .clang-format
fi

echo "Formatting Code"
find src -name '*.[ch]' | grep -v "gainspan" | grep -v "web_server" | grep -v "custom_timer" | grep -v "wireless_interface" | xargs clang-format -i $FOPTS
