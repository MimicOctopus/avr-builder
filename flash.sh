#!/usr/bin/env bash

source env_setup.sh
source .config.sh # The config script

if [[ -z $PLATFORM ]]; then
    echo "Platform has not been setup"
    exit
fi

if [[ -z $DEVICE ]]; then
    echo "User specific config not valid"
    echo "Please consult README"
    exit
fi

# Do setup 
AVRDUDE="../3rdParty/tools/arduino-1.0.6-$PLATFORM/hardware/tools/avrdude"
DUDEOPTS="-pm2560 -cwiring"
DUDECONF="../3rdParty/tools/arduino-1.0.6-$PLATFORM/hardware/tools/avrdude.conf"

if [[ $PLATFORM == $WINDOWS ]]; then
    #AVRDUDE=$AVRDUDE".exe"
    AVRDUDE="../3rdParty/tools/arduino-1.0.6-$PLATFORM/hardware/tools/avr/bin/avrdude.exe"
    DUDECONF="../3rdParty/tools/arduino-1.0.6-$PLATFORM/hardware/tools/avr/etc/avrdude.conf"
fi

cd build
$AVRDUDE -v -v $DUDEOPTS -P$DEVICE -b115200 -Uflash:w:SEG4545_Project.hex:a -C$DUDECONF

