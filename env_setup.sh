## Source this file to adapt the Environment of your running shell

echo "Generating dev build environment..."

CWD=`pwd`

TOOL_DIR="3rdParty/tools"

export WINDOWS="windows"
export LINUX="linux"

SYSTEM=`uname`

if [[ $SYSTEM == *"Linux"* ]]; then #Linux
    export PLATFORM=$LINUX
    BINGCC='bin.gcc'
elif [[ $SYSTEM == *"MINGW"* ]]; then
    export PLATFORM=$WINDOWS
    BINGCC='bin'
else
    echo "++UNSUPPORTED PLATFORM++"
    exit
fi

AVR_FILE="arduino-1.0.6-$PLATFORM.tar.bz2"
AVR_BASE="$TOOL_DIR/arduino-1.0.6-$PLATFORM"
    
AVR_LINK="https://github.com/Faeriol/avr-builder/releases/download/base/$AVR_FILE"

mkdir -p $TOOL_DIR
if [[ ! -d $AVR_BASE ]]; then
   cd $TOOL_DIR
   echo "Grabbing Arduino Toolset: $AVR_LINK"
   curl -O $AVR_LINK --location
   tar -xf $AVR_FILE
   rm -f $AVR_FILE
   cd $CWD
fi


export AVR="$CWD/$AVR_BASE/hardware/tools/avr"
AVRBIN="$AVR/bin"
AVRGCC="$AVR/$BINGCC"

export LD_LIBRARY_PATH="$AVR/lib"
export PATH=$AVRGCC:$AVRBIN:$PATH

echo "...done"
